Getting Started
===============

So, you are here. Welcome. This is the Advanced PostGIS Workshop. If you have no idea what PostGIS is or why you are here, maybe you should take a look at [a workshop that caters to beginners.](http://www.github.com/astroidex/). If you are still here, lets have some fun!! 

About Aucindown
---------------

We are a small company based in Kingston, Jamaica, that 



Sanity Checks
-------------

If you are using the workshop as intended, i.e., using the containers specified in the repository, or if you used the country hexagons interface, then everything should just work. Lets do some sanity checks:

```SQL 
SELECT version();
```

This should return 
 `PostgreSQL 15.3 (Debian 15.3-1.pgdg120+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 12.2.0-14) 12.2.0, 64-bit`

Next, we shall check on the existense of the PostGIS extension:

```SQL
SELECT postgis_full_version();
```

This should return:

`POSTGIS="3.3.3 2355e8e" [EXTENSION] PGSQL="150" GEOS="3.11.1-CAPI-1.17.1" SFCGAL="SFCGAL 1.4.1, CGAL 5.5.1, BOOST 1.74.0" PROJ="9.1.1" GDAL="GDAL 3.6.2, released 2023/01/02" LIBXML="2.9.14" LIBJSON="0.16" LIBPROTOBUF="1.4.1" WAGYU="0.5.0 (Internal)" RASTER`

If you are not using the recommended containers, you can still do all the exercises once these versions of PostgreSQL & PostGIS are installed. You may have to manually download some files and change the path locations in some of the exercises. 



Downloads
---------

Now that the infrastructure appears to be working, we need some data use.
Run the following to get started:

```SQl
CALL advanced_postgis_workshop();
```



If you are greeted with a message that implies success, you can hop over to do a [Quick-Recap](./01-Quick-Recap.md)
