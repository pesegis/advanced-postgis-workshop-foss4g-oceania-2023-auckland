Rasterman Vibrations: Seventh Dredging
=======================================================

## The Scenario: Catastrophic Cyclone

A category 5 hurricane, packing winds in excess of 245 Kph has just ploughed threw the island of Jamaica. Relief efforts are being hampered because the shipping lanes in Kingston Harbour appear to be partially obstructed by large volumes of silt. A rapid bathymetric survey of a portion of the harbour has just been completed. You are to determine the scope of the obstruction and the volume of any material that needs to be dredged to clear the lanes. The shipping lanes have a minimum depth of 17m. In addition, the folks over at hydrology want to get the total depth change and general shape of blockage.

### Background

Kingston harbour is the 7th largest natural harbour in the world and a vital trans-shipment port for vessels transiting the Panama canal. As such the Government of Jamaica commits considerable resources to maintaing the harbour. Detailed bathymetric surveys are conducted every 4 years. All the relevant data are located in the `/files/seventh` folder. These are:

 - `KGN_7th_RBS.csv` A csv file with the Rapid Bathymetric Survey of the area of interest.
 - `seventh_bathymetry.tif` The most recent bathymetric survey of the harbour.
 - `shipping_lane.wkt` A polygon of the shipping lane

## Rasters in your DB: Is this a good idea?

There are pros & cons and lot of articles on the web. But this is a Postgis workshop, so we're gonna stuff them in.


## Getting the data into your database (or not)

Postgis can store rater data in one of two ways: in-db or out-db. In-db is just as it sounds, everything is in a table in the database. Out-db stores some metadata in the database and keeps pointers to the raster files. Overview are always stored in the database. Overviews are particularly usefull for visualization purposes. They are lower resolution versions of your raster. In this exercise, ww will be using in-db storage.

## The Solution:

Lets itemize the tasks:
 0. We need to import the original bathymetry data.
 1. we need to import the RBS data.
 2. We need to import the shiping lane data.
 2. convert the RBS data to a raster.
 3. see how much of the raster in the shipping lanes are above the 16m mark for the dredging.
 4. See the difference in height for the hydrology folks.

### Importing the data

As always, we like to use schemas to separate our data.

```SQL
CREATE SCHEMA seventh;
```

We will import the original bathymetry data for the harbour. First we will drop into the container by doing:

```sh
docker exec -it advanced-postgis-workshop_pgsql_1 bash
```

Then we use the raster2pgsql program to pull our raster data into the database:

```sh
# in-db with automatic tiling & overviews @ levels 8 & 16
raster2pgsql -t 32x32 -l 16,32 -I -F -Y 10000 /files/seventh/seventh_bathymetry.tif seventh.bathymetry | psql -p 44449 -U workshop
```

Let us turn our attention to the Rapid Bathymetric Survey data. It is in a csv format. We will create a table that mirrors the csv file:

```SQL
CREATE TABLE seventh.post_cyclone_rbs(
  easting numeric(11,1),
  northing numeric(11,1),
  depth_cm bigint
);
```

Next, we will copy the data into the table using PostgreSQL's `COPY` command;

```SQL
COPY seventh.post_cyclone_rbs
FROM '/files/seventh/KGN_7th_RBS.csv'
WITH (FORMAT CSV, HEADER TRUE);
```

Next, we have the shipping lane data as a wkt file. Let us create a table and import this data as well:


```SQL
CREATE TABLE seventh.shipping_lane (wkt text);

-- copy the data 
COPY seventh.shipping_lane
FROM '/files/seventh/shipping_lane.wkt';
```

Ok, great now all the relevant data are in the the `seventh` schema. We now need to generate a raster table from this data. There are several steps to do, and we can bundle them up into one query, or do a separate query for each section. Lets do both, the latter so you can see all the individual steps, and the former to show that this can be done in one fell swoop. Even when building large queries, the strategic use of comments will make it easy to follow. We will be using PostGIS' `st_interpolateraster` function. This function takes a geometry and uses the Z value to generate a surface based on the various arguments supplied. Let us modify the `seventh.post_cyclone_rbs` table and add a geometry column and then populate it.

```SQL
/* This bit adds the column */
ALTER TABLE seventh.post_cyclone_rbs
ADD COLUMN geom geometry(PointZ, 3448);
/* This bit updates the newly added column */
UPDATE seventh.post_cyclone_rbs
SET geom = st_setsrid(st_makepoint(easting, northing, depth_cm), 3448);
```
The `st_interpolateraster` function takes at least 3 inputs:
 1. the data to be interpolated.
 2. the type of interpolation you want to do as a bit of text.
 3. a raster template so that it knows what kind of raster to return.

After each step, feel free to add the table to QGIS to have a look at what the output is.

Step 1: Create a single geometry for use in the `st_interpolateraster` function:

```SQL
CREATE TABLE seventh.pcd_rbs_1 AS
SELECT st_union(geom) geom FROM seventh.post_cyclone_rbs ;
```

Step 2: We're going to limit our analysis to the are of the RBS. So we need a polygon of just this area. We can use `st_concavehull` to generate a polygon that is a shrinkwrap of the RBS points:

```SQL
CREATE TABLE seventh.pcd_rbs_2 AS
SELECT 1 fid, st_concavehull(st_union(geom),0.05) geom FROM seventh.post_cyclone_rbs;
```

Step 3: The `st_interpolateraster` requires us to specify the dimensions of the raster that will be created, we do this by supplying a template raster. Since we want all our analysis to be against the existing bathymetry data, we can use this a template, we could however create a brand new raster with the dimensions and resolution of our choice. Also, remember we are only interested in the are of the RBS, so we will clip the bathymetry data to the concave hull we created earlier:

```SQL
CREATE TABLE seventh.pcd_rbs_3 AS
SELECT st_clip(rast, geom) tmpl8 FROM seventh.bathymetry
JOIN seventh.pcd_rbs_2 ON st_intersects(geom, rast);
```

Step 4: Do the actual interpolation. The second argument specifies the type of interpolation we want to use. 

```SQL
CREATE TABLE seventh.pcd_rbs_4 AS
SELECT st_setsrid(st_interpolateraster(
  (SELECT geom FROM seventh.pcd_rbs_1), --geometry to interpolate 
  'invdist:smoothing:2.0:radius:150:nodata:-1',
  (SELECT st_union(tmpl8) FROM seventh.pcd_rbs_3)
  ), 3448) rast, 1 rid;
```

Ok, you may have notice that it interpolated all the way to the bounding box of the input data, in our case, this is not what we want. So we need to use `st_clip` once more.

Step 5: `st_clip` the interpolated data:

```SQL 
CREATE TABLE seventh.pcd_rbs_5 AS
SELECT st_clip(rast, geom) rast FROM seventh.pcd_rbs_4
JOIN seventh.pcd_rbs_2 ON st_intersects(geom, rast);
```
So, now we have the interpolated data in the form we want. Next we wil start to *actually* answering the questons that were posed. All that came before was just setting up. So recall that the folks in hydrology just want the difference in depth between the two data sets. We can use the `st_mapalgebra` function to good effect here.

Step 6: Satisfy the hydrology folks:

```SQL
CREATE TABLE seventh.for_hydrology AS
SELECT st_mapalgebra(b4.rast, af.rast, '[rast1.val] - [rast2.val]') rast
FROM seventh.bathymetry b4
CROSS JOIN seventh.pcd_rbs_5 af
```

Ok, that takes care of the hydrology team, now for the dredging folks. We need use the shipping lane data to get an idea of how much material needs to be moved to reopen the shipping lanes. Recall that the ships entering the lane need a minimumdepth of 17m.

Step 7: Satisfying the dredging folks.

```SQL
CREATE TABLE seventh.for_dredging AS
WITH lane AS (SELECT st_geomfromewkt(wkt) lane FROM seventh.shipping_lane)
lane_rast AS (
  SELECT st_clip(rast, lane) rast FROM
  seventh.pcd_rbs_5, lane
  )
SELECT st_mapalgebra(af.rast,'32BSI', '1700 - [rast]') rast
FROM lane_rast;
```

Now you may notice some negative values in this new table. And we don't need those, lets change all negative values to the nodata value.

```SQL
UPDATE seventh.for_dredging
SET rast = st_reclass(rast, 1, '[-9999-0]:-9999,1-5000:1-5000', '32BSI');
```




